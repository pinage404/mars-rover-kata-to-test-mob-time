# TypeScript with formatting and test (Jest) on NodeJS

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#WORKDIR=node_typescript_jest/https://gitlab.com/pinage404/nix-sandboxes)

Or with [Nix](https://nixos.org)

```sh
nix flake new --template "gitlab:pinage404/nix-sandboxes#node_typescript_jest" ./your_new_project_directory
```
