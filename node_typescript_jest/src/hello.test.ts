/*
position, orientation
F B forward backward
L R left right
string -> position
*/

const toto = (commands: string): string => {
  const chouette = commands.split("")
  if (chouette.length === 2 && chouette[0] === "F" && chouette[1] === "F") {
    return `0,${commands.length} N`
  }
  if (chouette.length === 1 && chouette[0] === "F") {
    chouette.pop()
    return toto(chouette.join(""))
  }
  return `0,${commands.length} N`
}

it("test", () => {
  expect(toto("")).toEqual("0,0 N")
})

it("test2aaaaaaahhhhh!!!!!!!!!!!!horrible!!!!!!!!!", () => {
  expect(toto("F")).toEqual("0,1 N")
})

it("test3", () => {
  expect(toto("FF")).toEqual("0,2 N")
})
