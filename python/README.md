# Python with formatting, typing and test

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#WORKDIR=python/https://gitlab.com/pinage404/nix-sandboxes)

Or with [Nix](https://nixos.org)

```sh
nix flake new --template "gitlab:pinage404/nix-sandboxes#python" ./your_new_project_directory
```
