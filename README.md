# Nix Sandboxes

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

Sandboxes / starters for differents languages

## Requirements

* Install [Git](https://git-scm.com)
* Install [Nix](https://nixos.org)
* Install [DirEnv](https://direnv.net)

## Usage

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/pinage404/nix-sandboxes)

1. There is 2 possibles ways
    * **Complete**
        * Clone this repository

          ```sh
          git clone https://gitlab.com/pinage404/nix-sandboxes.git
          cd nix-sandboxes
          ```

        * Move to the directory

          e.g. for Rust

          ```sh
          cd rust
          ```

    * **Lighten / Faster**
        * Take just one langage

          e.g. for Rust

          ```sh
          nix flake new --template "gitlab:pinage404/nix-sandboxes#rust" ./your_new_project_directory
          ```

        * Move to the directory

          ```sh
          cd ./your_new_project_directory
          ```

1. Enable DirEnv

    This will trigger Nix to get and setup everything that is needed

    ```sh
    direnv allow
    ```

1. *Optional steps*
    * If you use VSCode
        * Install [VSCode](https://code.visualstudio.com)
        * Open from the command line in the folder

          ```sh
          code .
          ```

        * Install recommanded extensions

    * If you use Cachix
        * Install [Cachix](https://www.cachix.org/)
        * Use Nix's cache

          ```sh
          cachix use pinage404-nix-sandboxes
          ```

    * If you use [`git-gamble`](https://gitlab.com/pinage404/git-gamble/-/blob/main/README.md) : a tool that blend TCR + TDD to make sure to **develop** the **right** thing, **babystep by babystep**
        * Install [`git-gamble`](https://gitlab.com/pinage404/git-gamble/-/blob/main/README.md#how-to-install)
            * If you cloned the repository, it's already included
        * Tests should pass

          ```sh
          git gamble --pass
          ```

1. Enjoy

---

Fork of [`FaustXVI/sandboxes`](https://github.com/FaustXVI/sandboxes.git)
