# Prolog with test

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#WORKDIR=prolog/https://gitlab.com/pinage404/nix-sandboxes)

Or with [Nix](https://nixos.org)

```sh
nix flake new --template "gitlab:pinage404/nix-sandboxes#prolog" ./your_new_project_directory
```
