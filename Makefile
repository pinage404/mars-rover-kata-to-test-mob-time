DIRENV_NIX_EXEC := direnv exec . nix develop --command
LANGUAGES := $(wildcard */.)


update_all: update_self $(LANGUAGES)


$(LANGUAGES):
	{ \
	LANGUAGE="$(shell basename $(shell realpath "$@"))" ;\
	MESSAGE="$$LANGUAGE: update dependencies" ;\
	if [ -f "$@/Makefile" ]; then \
		pushd "$@" ;\
		direnv allow ;\
		$(DIRENV_NIX_EXEC) \
			make update ;\
		$(DIRENV_NIX_EXEC) \
			git-gamble --pass --message "$$MESSAGE" || true ;\
	fi \
	}


update_self:
	nix flake update
	$(DIRENV_NIX_EXEC) \
		git-gamble --pass --message 'base: update dependencies' -- true || true


warm_nix_cache:
	for LANGUAGE in . $(LANGUAGES); do \
		pushd "$$LANGUAGE" ;\
		direnv allow ;\
		$(DIRENV_NIX_EXEC) true ;\
		popd ;\
	done


.PHONY: update_all $(LANGUAGES)
