{
  description = "Nix with formatting, linting and test";

  # broken on Fish
  # as a workaround, use `nix develop` the first time
  # https://github.com/direnv/direnv/issues/1022
  nixConfig.extra-substituters = [
    "https://pinage404-nix-sandboxes.cachix.org"
  ];
  nixConfig.extra-trusted-public-keys = [
    "pinage404-nix-sandboxes.cachix.org-1:5zGRK2Ou+C27E7AdlYo/s4pow/w39afir+KRz9iWsZA="
  ];

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  inputs.nixt.url = "github:nix-community/nixt";
  inputs.nixt.inputs.nixpkgs.follows = "nixpkgs";
  inputs.nixt.inputs.flake-utils.follows = "flake-utils";

  outputs = { self, nixpkgs, flake-utils, nixt }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
      in
      {
        devShell = pkgs.mkShell {
          packages = [
            pkgs.nixpkgs-fmt
            pkgs.statix
            # pkgs.nix-linter # broken
            nixt.packages."${system}".nixt

            pkgs.rnix-lsp # needed by VSCode extension
          ];
        };
      });
}
