#!/usr/bin/env sh

hello() {
	NAME="${1:-world}"
	echo "Hello ${NAME}"
}

test_hello_world() {
	assertEquals "$(hello)" "Hello world"
}

test_hello_foo() {
	assertEquals "$(hello 'foo')" "Hello foo"
}
